<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>
<body>
    <h1>Mengubah huruf</h1>
    <?php
        function ubah_huruf($string){
        //kode di sini
            $output="";
            foreach(str_split($string) as $alp){
                $output .= ++$alp;
            }
            echo "Kata yang diubah adalah ".$string." menjadi : ".$output;
            echo "<br>";
        }

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>
</html>